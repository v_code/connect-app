package com.example.connectapp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Logo {
    private static final List<String> words = new ArrayList<>(Arrays.asList(
            "You look good! A real dinosaur!",
            "Hi, how are you? I'm dinosaur-fantastic!",
            "What's happened? Have nothing to do?",
            "Come on, try my walkie-talkie!",
            "Yes, you are stubborn! Click not on me, but on the green buttons!",
            "This quest is called \"Find the Green Button\"! You lose!",
            "The world would be a better place if everyone was like you"
    ));

    public static String getRandomWord(){
        return words.get(getRandomNumber(words.size()-1));
    }

    private static int getRandomNumber(int n) {
        return new Random().nextInt(n + 1);
    }


}

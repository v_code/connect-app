package com.example.connectapp.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;

public class TutorialDialogFragment extends DialogFragment{
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        return builder
                .setTitle("Welcome to Connectosaur!")
                .setMessage("Requires device A and B with Bluetooth enabled. On device A, press the \"Connect\" -> \"Server\" button. On device B, press the \"Paired\" button and select device A. If it is not there, return to the first screen (\"Come back\" button). Then click \"Discover\" and select the device to pair and repeat the above.")
                .setPositiveButton("Thanks, I've got it!", null)
                .create();
    }
    public static void clickNav( FragmentManager fragmentManager ){
        TutorialDialogFragment dialog = new TutorialDialogFragment();
        dialog.show( fragmentManager, "tutorial" );
    }
}

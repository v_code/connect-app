package com.example.connectapp;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class ComponentUI {
    public static void changeConnectionUI(Button btnConnect, Button btnDisconnect, FloatingActionButton btnAudio, TextView titleSection){
        btnConnect.setEnabled(false);
        btnDisconnect.setEnabled(true);
        btnAudio.setVisibility(View.VISIBLE);
        titleSection.setVisibility(View.VISIBLE);
        titleSection.setText(R.string.audio_title);
    }

    public static void changeDisconnectUI(Button btnConnect, Button btnDisconnect, FloatingActionButton btnAudio, TextView titleSection){
        btnConnect.setEnabled(true);
        btnDisconnect.setEnabled(false);
        btnAudio.setVisibility(View.GONE);
        titleSection.setVisibility(View.GONE);
    }

}

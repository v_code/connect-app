package com.example.connectapp.activity;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.connectapp.MainActivity;
import com.example.connectapp.R;
import com.example.connectapp.Logo;

import java.util.ArrayList;

public class DiscoverActivity extends AppCompatActivity implements Activity {
    public TextView title;
    public ListView deviceListView;
    public BluetoothAdapter bluetoothAdapter = MainActivity.bluetoothAdapter;
    private final ArrayList<BluetoothDevice> deviceList = new ArrayList<>();

    /**
     * The BroadcastReceiver for paired devices
     */
    private final BroadcastReceiver broadcastPair = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if (ActivityCompat.checkSelfPermission(DiscoverActivity.this, android.Manifest.permission.BLUETOOTH_CONNECT) != PackageManager.PERMISSION_GRANTED) {
                    if (device.getBondState() == BluetoothDevice.BOND_BONDED) {
                        Toast.makeText(getApplicationContext(), R.string.device_paired + device.getName(), Toast.LENGTH_SHORT).show();
                        unregisterReceiver(this);
                    }
                }
            }
        }
    };

    /**
     * The BroadcastReceiver for discovered devices
     */
    private final BroadcastReceiver broadcastDiscover = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if (ActivityCompat.checkSelfPermission(DiscoverActivity.this, android.Manifest.permission.BLUETOOTH_CONNECT) != PackageManager.PERMISSION_GRANTED) {
                    if (!deviceList.contains(device) && device != null && device.getBondState() != BluetoothDevice.BOND_BONDED) {
                        deviceList.add(device);
                        ArrayAdapter<BluetoothDevice> adapter = createAdapter();
                        deviceListView.setAdapter(adapter);
                        deviceListView.setOnItemClickListener((parent, view, position, id) -> makePairing( parent, position ));
                    }
                }
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                Toast.makeText(DiscoverActivity.this, R.string.search_completed, Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discover);
        defineView();
        go();
        createBroadcastDiscover();
    }

    @Override
    public void defineView() {
        title = findViewById(R.id.title);
        deviceListView = findViewById(R.id.listView);
        title.setText(R.string.title_discovered_devices);
    }

    @Override
    public void onClickLogo(View v) {
        Toast.makeText(getApplicationContext(), Logo.getRandomWord(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClickNav(View v) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    public void go(){
        deviceList.clear();
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.BLUETOOTH_SCAN) != PackageManager.PERMISSION_GRANTED) {
            if (bluetoothAdapter.isDiscovering()) {
                bluetoothAdapter.cancelDiscovery();
            }
            bluetoothAdapter.startDiscovery();
            Toast.makeText(getApplicationContext(), R.string.find_discovered_devices, Toast.LENGTH_SHORT).show();
        }
    }

    private void createBroadcastDiscover(){
        IntentFilter filterDiscovery = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(broadcastDiscover, filterDiscovery);
        filterDiscovery = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        registerReceiver(broadcastDiscover, filterDiscovery);
    }



    private void makePairing(AdapterView<?> parent, int position) {
        BluetoothDevice device = (BluetoothDevice) parent.getItemAtPosition(position);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.BLUETOOTH_SCAN) != PackageManager.PERMISSION_GRANTED) {
            if (bluetoothAdapter.isDiscovering()) {
                bluetoothAdapter.cancelDiscovery();
            }
            if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
                device.createBond();
            } else {
                Toast.makeText(getApplicationContext(), "Device already paired: " + device.getName(), Toast.LENGTH_SHORT).show();
            }
        }
        createBroadcastPair();
    }

    private void createBroadcastPair(){
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
        registerReceiver(broadcastPair, filter);
    }

    private ArrayAdapter<BluetoothDevice> createAdapter() {
        return new ArrayAdapter<BluetoothDevice>(DiscoverActivity.this, android.R.layout.simple_list_item_2, android.R.id.text1, deviceList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView text1 = view.findViewById(android.R.id.text1);
                TextView text2 = view.findViewById(android.R.id.text2);

                BluetoothDevice device = getItem(position);
                if (ActivityCompat.checkSelfPermission(DiscoverActivity.this, android.Manifest.permission.BLUETOOTH_CONNECT) != PackageManager.PERMISSION_GRANTED) {
                    text1.setText(device.getName());
                    text2.setText(device.getAddress());
                }
                return view;
            }
        };
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastDiscover);
    }
}
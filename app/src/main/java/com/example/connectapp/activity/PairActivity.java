package com.example.connectapp.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.connectapp.MainActivity;
import com.example.connectapp.R;
import com.example.connectapp.Logo;

import java.util.ArrayList;
import java.util.Set;

public class PairActivity extends AppCompatActivity implements Activity {
    public TextView title;
    public ListView deviceListView;
    public BluetoothAdapter bluetoothAdapter = MainActivity.bluetoothAdapter;
    private final ArrayList<BluetoothDevice> deviceList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pair);
        defineView();
        go();
    }

    @Override
    public void defineView() {
        title = findViewById(R.id.title);
        deviceListView = findViewById(R.id.listView);
        title.setText(R.string.title_paired_devices);
    }

    public void go(){
        deviceList.clear();
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.BLUETOOTH_CONNECT) != PackageManager.PERMISSION_GRANTED) {
            Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();
            assert pairedDevices != null;
            if (pairedDevices.size() > 0) {
                deviceList.addAll(pairedDevices);
            }
            Toast.makeText(getApplicationContext(), R.string.find_paired_devices, Toast.LENGTH_SHORT).show();
        }

        deviceListView.setAdapter( createAdapter() );
        deviceListView.setOnItemClickListener((parent, view, position, id) -> {
            if (bluetoothAdapter.isEnabled()) {
                Bundle extra = new Bundle();
                extra.putSerializable("deviceList",deviceList);
                Intent intent = new Intent(getBaseContext(), AudioActivity.class);
                intent.putExtra("nameActivity","PairActivity");
                intent.putExtra("extra",extra);
                intent.putExtra("position",position);
                startActivity(intent);
            } else {
                Toast.makeText(getApplicationContext(), R.string.bluetooth_required, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private ArrayAdapter<BluetoothDevice> createAdapter() {
        return new ArrayAdapter<BluetoothDevice>(getApplicationContext(), android.R.layout.simple_list_item_2, android.R.id.text1, deviceList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView text1 = view.findViewById(android.R.id.text1);
                TextView text2 = view.findViewById(android.R.id.text2);
                BluetoothDevice device = getItem(position);
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.BLUETOOTH_CONNECT) != PackageManager.PERMISSION_GRANTED) {
                    text1.setText(device.getName());
                    text2.setText(device.getAddress());
                }
                return view;
            }
        };
    }

    @Override
    public void onClickLogo(View v) {
        Toast.makeText(getApplicationContext(), Logo.getRandomWord(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClickNav(View v) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
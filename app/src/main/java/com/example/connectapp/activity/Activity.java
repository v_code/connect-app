package com.example.connectapp.activity;

import android.view.View;

public interface Activity {
    void defineView();
    void onClickLogo(View v);
    void onClickNav(View v);
}

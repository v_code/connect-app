package com.example.connectapp.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.connectapp.Logo;
import com.example.connectapp.MainActivity;
import com.example.connectapp.R;
import com.example.connectapp.ComponentUI;

import com.example.connectapp.audio.Talk;

import com.example.connectapp.audio.threads.AcceptThread;
import com.example.connectapp.audio.threads.ConnectThread;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.UUID;

public class AudioActivity extends AppCompatActivity implements Activity {
    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 200;
    private boolean permissionToRecordAccepted = false;
    private final String [] permissions = {Manifest.permission.RECORD_AUDIO};
    private Button server;
    private Button disconnect;
    private TextView title;
    private FloatingActionButton audio;
    public static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
    private final BluetoothAdapter bluetoothAdapter = MainActivity.bluetoothAdapter;
    private Talk talk;
    private AcceptThread acceptThread;
    private ConnectThread connectThread;
    private BluetoothSocket socket;
    private boolean isServer = false;
    private boolean isClient = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio);
        ActivityCompat.requestPermissions(this, permissions, REQUEST_RECORD_AUDIO_PERMISSION);
        defineView();
        hideElem();

        talk = new Talk();

        Bundle arguments = getIntent().getExtras();
        String nameActivity = arguments.get("nameActivity").toString();
        if ( nameActivity.equals("PairActivity")) {
            Bundle extra = getIntent().getBundleExtra("extra");
            ArrayList<BluetoothDevice> deviceList = (ArrayList<BluetoothDevice>) extra.getSerializable("deviceList");
            Bundle arg = getIntent().getExtras();
            int position = Integer.parseInt(arg.get("position").toString());

            BluetoothDevice device = bluetoothAdapter.getRemoteDevice(deviceList.get(position).getAddress());
            connectThread = new ConnectThread(device, this);

            if (connectThread.run(this)){
                socket = connectThread.getMmSocket();
                talk.audioFunctions(socket);
                isClient = true;
                ComponentUI.changeConnectionUI(server, disconnect, audio, title);
                Toast.makeText(getApplicationContext(), "Connection was successful", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Connection failed", Toast.LENGTH_LONG).show();
            }
        }
        audio.setOnTouchListener((v, event) -> onClickAudio(event));
    }

    public void makeServer(View v){
        if (bluetoothAdapter.isEnabled()) {
            acceptThread = new AcceptThread(this);
            acceptThread.run();
            socket = acceptThread.getSocket();
            talk.audioFunctions(socket);
            ComponentUI.changeConnectionUI(server, disconnect, audio, title);
            Toast.makeText(getApplicationContext(), "Connection was successful", Toast.LENGTH_LONG).show();
            isServer = true;
        } else { Toast.makeText(getApplicationContext(), R.string.bluetooth_required, Toast.LENGTH_SHORT).show(); }
    }

    public void disconnectConnection(View v){
        if (isServer) {
            acceptThread.cancel();
            isServer = false;
        }
        if (isClient) {
            connectThread.cancel();
            isClient = false;
        }
        talk.audioRelease();
        ComponentUI.changeDisconnectUI(server,disconnect, audio,title);
        Toast.makeText(getApplicationContext(), "Disconnect", Toast.LENGTH_LONG).show();
    }

    private boolean onClickAudio(MotionEvent event){
        int action = event.getAction();
        if (action == MotionEvent.ACTION_DOWN ) {
            talk.stopPlay();
            talk.startRecord();
        } else if (action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_CANCEL ) {
            talk.stopRecord();
            talk.startPlay();
        }
        return false;
    }

    @Override
    public void defineView() {
        server = findViewById(R.id.server);
        disconnect = findViewById(R.id.disconnect);
        title = findViewById(R.id.title);
        audio = findViewById(R.id.audio);
    }

    private void hideElem() {
        disconnect.setEnabled(false);
        audio.setVisibility(View.GONE);
        title.setVisibility(View.GONE);
    }

    @Override
    public void onClickLogo(View v) {
        Toast.makeText(getApplicationContext(), Logo.getRandomWord(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClickNav(View v) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_RECORD_AUDIO_PERMISSION) {
            permissionToRecordAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
        }
        if (!permissionToRecordAccepted ) finish();
    }

}

package com.example.connectapp.audio.elements;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;

public class Track extends MainAudio {
    public AudioTrack getAudioTrack(int sampleRate) {
        return new AudioTrack(AudioManager.STREAM_MUSIC,
                sampleRate, AudioFormat.CHANNEL_OUT_MONO,
                AudioFormat.ENCODING_PCM_16BIT, getBufferSize(sampleRate), AudioTrack.MODE_STREAM);
    }
}

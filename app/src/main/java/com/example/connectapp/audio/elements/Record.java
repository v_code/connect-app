package com.example.connectapp.audio.elements;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;

public class Record extends MainAudio {
    public AudioRecord getAudioRecord(int sampleRate) {
        return new AudioRecord(MediaRecorder.AudioSource.MIC, sampleRate,
                AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT,
                getBufferSize(sampleRate));
    }
}

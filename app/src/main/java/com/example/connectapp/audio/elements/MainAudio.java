package com.example.connectapp.audio.elements;

import android.media.AudioFormat;
import android.media.AudioTrack;

public class MainAudio {
    public int getBufferSize(int sampleRate) {
        return AudioTrack.getMinBufferSize(sampleRate, AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT);
    }
}

package com.example.connectapp.audio;

import android.bluetooth.BluetoothSocket;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.example.connectapp.audio.elements.Record;
import com.example.connectapp.audio.elements.Track;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Talk extends AppCompatActivity {
    private BluetoothSocket socket = null;
    private AudioRecord audioRecord = null;
    private AudioTrack audioTrack = null;
    private InputStream inputStream;
    private OutputStream outputStream;
    private byte[] buffer = null;
    private byte[] playBuffer = null;
    private boolean isRecording = false;
    int sampleRate = 11025;
    Track track = new Track();
    Record record = new Record();
    int bufferSize = track.getBufferSize(sampleRate);

    public void audioFunctions(BluetoothSocket socket){
        audioCreate();
        setSocket(socket);
        setupInputStream();
        setupOutputStream();
        startPlay();
    }

    public void audioCreate() {
        audioTrack = track.getAudioTrack(sampleRate);
        audioRecord = record.getAudioRecord(sampleRate);
    }

    public void setSocket(BluetoothSocket socket) {
        this.socket = socket;
    }

    public void setupInputStream(){
        try {
            inputStream = socket.getInputStream();
        } catch (IOException e) {
            Log.e("Socket", "Input stream failed", e);
        }
    }

    public void setupOutputStream() {
        try {
            outputStream = socket.getOutputStream();
        } catch (IOException e) {
            Log.e("Socket", "Output stream failed", e);
        }
    }

    // ----------   action down   ----------

    public void stopPlay() {
        if (audioTrack != null) {
            isRecording = true;
            audioTrack.stop();
        }
    }

    public void startRecord() {
        buffer = new byte[bufferSize];
        audioRecord.startRecording();
        isRecording = true;
        Thread recordingThread = new Thread(this::sendRecord, "AudioRecorder Thread");
        recordingThread.start();
    }

    public void sendRecord() {
        while (isRecording) {
            try {
                audioRecord.read(buffer, 0, bufferSize);
                outputStream.write(buffer);
            } catch (IOException e) {
                Log.e("Recording", "Send record failed", e);
            }
        }
    }

    // ----------   action up or cancel   ----------
    public void stopRecord() {
        if (audioRecord != null) {
            isRecording = false;
            audioRecord.stop();
        }
    }

    public void startPlay() {
        playBuffer = new byte[bufferSize];
        audioTrack.play();
        Thread playThread = new Thread(this::getRecord, "AudioTrack Thread");
        playThread.start();
    }

    public void getRecord() {
        while (!isRecording) {
            try {
                if (inputStream.available() == 0) {
                    //Do nothing
                } else {
                    inputStream.read(playBuffer);
                    audioTrack.write(playBuffer, 0, playBuffer.length);
                }
            } catch (IOException e) {
                Log.e("Recording", "Get record failed", e);
            }
        }
    }

    public void audioRelease() {
        audioTrack.release();
        audioRecord.release();
    }

}

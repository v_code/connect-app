package com.example.connectapp;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.example.connectapp.activity.Activity;
import com.example.connectapp.activity.AudioActivity;
import com.example.connectapp.activity.DiscoverActivity;
import com.example.connectapp.activity.PairActivity;
import com.example.connectapp.fragment.TutorialDialogFragment;

public class MainActivity extends AppCompatActivity implements Activity {
    public Button onOff;
    public Button nav;
    public static BluetoothAdapter bluetoothAdapter;

    /**
     * The BroadcastReceiver on and off bluetooth
     */
    private final BroadcastReceiver broadcastOnOff = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                        Toast.makeText(MainActivity.this, R.string.bluetooth_is_off, Toast.LENGTH_SHORT).show();
                        setTextBtn();
                        break;
                    case BluetoothAdapter.STATE_ON:
                        Toast.makeText(MainActivity.this, R.string.bluetooth_is_on, Toast.LENGTH_SHORT).show();
                        setTextBtn();
                        break;
                }
            }
        }
    };

    /**
     * The ActivityResultLauncher for onOffBluetooth()
     */
    private final ActivityResultLauncher<Intent> activityOnOff = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == RESULT_CANCELED) {
                    Toast.makeText(MainActivity.this, R.string.action_canceled, Toast.LENGTH_SHORT).show();
                }
            });

    /**
     * The ActivityResultLauncher for becomeVisible()
     */
    private final ActivityResultLauncher<Intent> activityVisible = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == android.app.Activity.RESULT_OK) {
                    Toast.makeText(MainActivity.this, R.string.discoverable_on, Toast.LENGTH_SHORT).show();
                } else {
                    if (result.getResultCode() == RESULT_CANCELED) {
                        Toast.makeText(MainActivity.this, R.string.discoverable_off,
                                Toast.LENGTH_SHORT).show();
                    }
                }
            });

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        defineView();
        setBluetoothAdapter();
        setTextBtn();
        checkSupportBluetooth();
        createBroadcast();
    }
    @Override
    public void defineView() {
        nav = findViewById(R.id.nav);
        onOff = findViewById(R.id.onOff);
    }
    private void setBluetoothAdapter() {
        BluetoothManager bluetoothManager = getSystemService(BluetoothManager.class);
        bluetoothAdapter = bluetoothManager.getAdapter();
    }
    private void checkSupportBluetooth() {
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH) || bluetoothAdapter == null) {
            Toast.makeText(this, R.string.bluetooth_not_supported, Toast.LENGTH_SHORT).show();
        }
    }
    @SuppressLint("SetTextI18n")
    public void setTextBtn() {
        if (!bluetoothAdapter.isEnabled()) {
            onOff.setText(R.string.turn_on_bluetooth);
        } else {
            onOff.setText(R.string.turn_off_bluetooth);
        }
        nav.setText(R.string.read);
    }
    private void createBroadcast(){
        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(broadcastOnOff, filter);
    }
    @Override
    public void onClickLogo(View v) {
        Toast.makeText(getApplicationContext(), Logo.getRandomWord(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClickNav(View v) {
        TutorialDialogFragment.clickNav( getSupportFragmentManager() );
    }

    public void onOffBluetooth(View v) {
        if (!bluetoothAdapter.isEnabled()) {
            activityOnOff.launch(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE));
        } else {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.BLUETOOTH_CONNECT) != PackageManager.PERMISSION_GRANTED) {
                bluetoothAdapter.disable();
            }
        }
    }
    public void becomeVisible(View v) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.BLUETOOTH_ADVERTISE) != PackageManager.PERMISSION_GRANTED) {
            activityVisible.launch(new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE));
        }
    }
    public void openDiscoverActivity(View v) {
        if (bluetoothAdapter.isEnabled()) {
            Intent intent = new Intent(this, DiscoverActivity.class);
            startActivity(intent);
        } else {
            Toast.makeText(MainActivity.this, R.string.bluetooth_required, Toast.LENGTH_SHORT).show();
        }
    }
    public void openPairActivity(View v) {
        if (bluetoothAdapter.isEnabled()) {
            Intent intent = new Intent(this, PairActivity.class);
            startActivity(intent);
        } else {
            Toast.makeText(MainActivity.this, R.string.bluetooth_required, Toast.LENGTH_SHORT).show();
        }
    }
    public void openConnectActivity(View v) {
        Intent intent = new Intent(this, AudioActivity.class);
        intent.putExtra("nameActivity","MainActivity");
        startActivity(intent);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (bluetoothAdapter != null) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.BLUETOOTH_SCAN) != PackageManager.PERMISSION_GRANTED) {
                bluetoothAdapter.cancelDiscovery();
            }
        }
        unregisterReceiver(broadcastOnOff);
    }
}

# Connect App

Connect App (Connectosaur) is a java mobile application for interacting with Bluetooth on android devices. Walkie-talkie (push-to-talk) between two devices, and other activities with Bluetooth.

## Application features:

Visible - make the phone discoverable by other devices.

Turn on/off - enable/disable bluetooth.

Discover - find devices with bluetooth enabled (available for pairing).

Paired - display a list of paired devices.

Connect - walkie-talkie (connection between two phones, two-way audio transmission from a microphone and real-time playback).

Logo - display random phrases when you click on the logo.

Read - instructions for the application.

Come back - return to the first screen.

## Technologies

Java, xml, Activity, Fragment, BroadcastReceiver, Intent, Gradle, Toast, OOP (interface, inheritance).

![screen_connect_app_main.jpg](img/screen_connect_app_main.jpg) ![screen_connect_app_server.jpg](img/screen_connect_app_server.jpg)
